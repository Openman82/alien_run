﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;


namespace AlienRun
{
    class Level
    {
        //Data
        Tile[,] tiles;
        
        private const int LEVEL_WIDTH = 100;
        private const int LEVEL_HEIGHT = 100;
        private const int TILE_WIDTH = 70;
        private const int TILE_HEIGHT = 70;

        // Methods
        public void LoadContent(ContentManager content)
        {
            // cretaing a single copy of the tile texture that will be used by all tiles.
            Texture2D tileTexture = content.Load<Texture2D>("graphics/tiles/box");

            tiles = new Tile[LEVEL_WIDTH,LEVEL_HEIGHT];
            for (int x = 0;  x < LEVEL_WIDTH; ++x)
            {   
                
                
                for (int y = 0; y < LEVEL_HEIGHT; ++y)
                {
                    if (y == 3  || x == 1 && y == 2 || x == 8)
                    {
                        tiles[x, y] = new Tile(tileTexture, new Vector2(x * TILE_WIDTH, y* TILE_HEIGHT));
                    }
                    else
                    {
                        tiles[x, y] = null;
                    }

                    
                }
                
            }

            
        }

        public void CreateTile(int tileX, int tileY, Texture2D tileTexture )
        {
            Vector2 tilePosition = new Vector2(tileX * TILE_WIDTH, tileY * TILE_HEIGHT);
            Tile newTile = new Tile(tileTexture, tilePosition);
            tiles[tileX, tileY] = newTile;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            for (int x = 0; x < LEVEL_WIDTH; ++x)
            {
                
                for (int y = 0; y < LEVEL_HEIGHT; ++y)
                {
                        if (tiles[x, y] != null)
                        {
                        tiles[x, y].Draw(spriteBatch);
                        }

                }
            }
        }
    }
}
