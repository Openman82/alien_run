﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;

namespace AlienRun
{
    class Player
    {
        //----------------------------------------
        //Data(Not Public)
        //----------------------------------------

        Vector2 position = Vector2.Zero;
        Texture2D sprite = null;
        Vector2 velocity = Vector2.Zero;

        private const float MOVE_SPEED = 300.0f;
        private const float GRAVITY_ACCEL = 3400.0f;
        private const float TERMINAL_VELOCITY = 550.0f;
        //----------------------------------------
        //Methods (Public)
        //----------------------------------------
        public void LoadContent(ContentManager content)
        {
            sprite = content.Load<Texture2D>("graphics/player/player-stand");
        }


       
      
        
        public void Update(GameTime ourGameTime)
        {
            float deltaTime = (float)ourGameTime.ElapsedGameTime.TotalSeconds;



            velocity.X = MOVE_SPEED;

            velocity.Y += GRAVITY_ACCEL * deltaTime;

            velocity.Y = MathHelper.Clamp(velocity.Y, -TERMINAL_VELOCITY, TERMINAL_VELOCITY);



            // pos2 = pos1 + deltaPos
            //deltaPos = velocity * deltaTime
            position += velocity * deltaTime;



        }


        public void Draw (SpriteBatch ourSpriteBatch)
        {
            ourSpriteBatch.Draw(sprite, position, Color.White);


            
        }
        
        

    }
}
