﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;

namespace AlienRun
{
    class Tile
    {

        //----------------------------------------
        //Data(Not Public)
        //----------------------------------------
        private Texture2D tileSprite;
        private Vector2 tilePosition;


        // Methods

        public Tile(Texture2D newTexture, Vector2 newPosition)
        {
            tileSprite = newTexture;
            tilePosition = newPosition;
        }        
        

        
        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(tileSprite, tilePosition, Color.White);
        }





    }
}
